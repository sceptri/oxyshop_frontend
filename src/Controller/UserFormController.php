<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Constants;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\FormInterface;

class UserFormController extends AbstractController
{
	private $client;

	//We specify constructor to add http client
	public function __construct(HttpClientInterface $client)
	{
		$this->client = $client;
	}

	/**
	 * @Route("/", name="home")
	 */
	public function userForm(Request $request): Response
	{
		//Get show_all query parameter
		$showAll = (bool) $request->query->get('show_all');

		$users = "[]";

		//Only if we want to show all users we send GET request
		if($showAll) {
			//Perform API get request for all users
			$usersResponse = $this->client->request(
				'GET',
				Constants::API_URL.'/user/get-all'
			);

			//In case of error show it
			if($usersResponse->getStatusCode() != 200) {
				return $this->redirectToRoute("error", ["error" => $usersResponse->getContent(false)]);
			} else {
				$users = $usersResponse->getContent();
			}
		}

		//Make dummy user object
		$user = new User();

		//Make register form
		$userForm = $this->createRegisterForm($user);

		//Processing user form - first it's checked if it's been submitted and if it's valid
		$userForm->handleRequest($request);
		if($userForm->isSubmitted() && $userForm->isValid()) {
			$user = $userForm->getData();

			//If form is valid, we sent it to the API
			$registerResponse = $this->client->request(
				'POST',
				Constants::API_URL.'/user/register',
				[
					'body' => json_encode($user->toArray())
				]
			);

			//If request wasn't fine, redirect to error page
			if($registerResponse->getStatusCode() != 200) {
				return $this->redirectToRoute("error", ["error" => $registerResponse->getContent(false)]);
			}

			return $this->redirectToRoute('home', ["show_all" => true]);
		}

		//Create show all users button form
		$showAllForm = $this->createShowAllForm();

		$showAllForm->handleRequest($request);
		if($showAllForm->isSubmitted() && $showAllForm->isValid()) {
			return $this->redirectToRoute('home', ["show_all" => !$showAll]);
		}

		//Render homepage with registration form and show all users form
		return $this->render('home.html.twig', [
			"show_all" => $showAll,
			"users" => json_decode($users),
			"user_form" => $userForm->createView(),
			"show_all_form" => $showAllForm->createView()
		]);
	}

	//Using symfony forms create a form for user registration
	private function createRegisterForm(User $user): Form
	{
		return $this->createFormBuilder($user)
			->add('name', TextType::class, ['label' => 'Jméno'])
			->add('email', EmailType::class, ['label' => 'Email'])
			->add('password', PasswordType::class, ['label' => 'Heslo'])
			->add('permissions', ChoiceType::class, [
				'choices' => [
					"Uživatel" => Constants::UserCode,
					"Admin" => Constants::AdminCode
				],
				'label' => 'Oprávnění'
			])
			->add('save', SubmitType::class, ['label' => 'Zaregistrovat'])
			->getForm();
	}

	//Implement show all button using forms
	private function createShowAllForm(): Form
	{
		return $this->createFormBuilder()
			->add('show_all', SubmitType::class, ['label' => 'Zobrazit všechny uživatele?'])
			->getForm();
	}

	/**
	 * @Route("/error", name="error")
	 */
	public function showErrors(Request $request): Response
	{
		//Get error parameter if there is one, otherwise set error message to none errors
		$error = $request->query->get('error');
		if($error == null) {
			$error = "Žádná chyba";
		}

		//Render template with error message
		return $this->render('error.html.twig', [
			"error" => $error
		]);

	}

}